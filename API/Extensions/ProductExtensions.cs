
using API.Entities;

namespace API.Extensions;

public static class ProductExtensions
{
    // IQuerble no possede pas un filtre pour nos entitier du coups, on extend !
    // ya pas [HttpGet] du coups il va comprendre que c'est un QUERY STRING (pas de queryParm dans l'url)

    // Add sort 
    public static IQueryable<Product> Sort(this IQueryable<Product> query, string orderBy)
    {
        // Si ya pas de sorting return un sorting default by name
        if (string.IsNullOrWhiteSpace(orderBy)) return query.OrderBy(p => p.Name);
        query = orderBy switch
        {
            "price" => query.OrderBy(p => p.Price),
            "priceDesc" => query.OrderByDescending(p => p.Price),
            _ => query.OrderBy(p => p.Name)
        };
        return query;
    }

    public static IQueryable<Product> Search(this IQueryable<Product> query, string searchTerm)
    {
        // Si ya pas de sorting return un sorting default by name
        if (string.IsNullOrWhiteSpace(searchTerm)) return query;
        // supprimer les white spaces & miniscule
        var lowCaseSearchTerm = searchTerm.Trim().ToLower();
        // Miniscule notre product Name  
        return query.Where(p => p.Name.ToLower().Contains(lowCaseSearchTerm));
    }

    public static IQueryable<Product> Filter(this IQueryable<Product> query, string brands, string types)
    {
        var brandsList = new List<string>();
        var typesList = new List<string>();

        if (!string.IsNullOrEmpty(brands)) brandsList.AddRange(brands.ToLower().Split(",").ToList());

        if (!string.IsNullOrEmpty(types)) typesList.AddRange(types.ToLower().Split(",").ToList());

        // Checking if count == 0 mean no filter ! || elle va returner un query avec un tt les tt les brand dans brandsList
        query = query.Where(q => brandsList.Count == 0 || brandsList.Contains(q.Brand.ToLower()));
        query = query.Where(q => typesList.Count == 0 || typesList.Contains(q.Type.ToLower()));

        return query;
    }
}
