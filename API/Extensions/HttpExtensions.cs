using System.Text.Json;
using API.RequestHelpers;

namespace API.Extensions;

public static class HttpExtensions
{
    public static void AddPaginationHeader(this HttpResponse response,Metadata metaData){
        var options = new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase};
        response.Headers.Add("Pagination" , JsonSerializer.Serialize(metaData,options));
        // On voit Pagination sur Swagger car il a mm domaine, sur Client faut ajouter ce champs OBLIGATOIREMENT pour le recuperer niveau front
         response.Headers.Add("Access-Control-Expose-Headers" , "Pagination");
    }
}
