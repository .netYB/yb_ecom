using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace API.Middleware;

public class TestMidlleware
{
    private readonly RequestDelegate _next;
    public ILogger<TestMidlleware> _logger { get; }
    public TestMidlleware(RequestDelegate next, ILogger<TestMidlleware> logger)
    {
        _logger = logger;
        _next = next;
    }
    public async Task InvokeAsync(HttpContext context)
    {
        try
        {
            _logger.LogInformation("Hello Si Hyrkul ",
            DateTime.UtcNow.ToLongTimeString());
            await _next(context);
        }
        catch (Exception ex)
        {
           
        }
    }
}
