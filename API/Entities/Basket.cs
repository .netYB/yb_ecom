namespace API.Entities;

public class Basket
{
    public int Id { get; set; }
    public string BuyerId { get; set; }
    public List<BasketItem> Items { get; set; } = new();
    public void AddItem(Product product, int quantity)
    {
        if (Items.All(item => item.ProductId != product.Id))
        {
            Items.Add(new BasketItem { Product = product, Quantity = quantity });
             return;
        }

        var existingItem = Items.FirstOrDefault(item =>  item.ProductId == product.Id);
        if(existingItem !=null) existingItem.Quantity += quantity;
    }
    public void RemoveItem(Product product, int quantity)
    {
        var Item = Items.FirstOrDefault(item => item.ProductId == product.Id);
        if(Item ==null) return;
        Item.Quantity -= quantity;
        if(Item.Quantity ==0) Items.Remove(Item);
    }
}
