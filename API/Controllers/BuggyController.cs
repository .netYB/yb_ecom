
using Microsoft.AspNetCore.Mvc;

namespace API.Controllers;

public class BuggyController : BaseApiController
{
    [HttpGet("not-found")]
    public ActionResult GetNotFound()
    {
        return NotFound();
    }
    [HttpGet("bad-request")]
    public ActionResult GetBadRequest()
    {
        return BadRequest(new ProblemDetails { Title = "This is bad request si Hyrkul" });
    }
    [HttpGet("unauthorise")]
    public ActionResult GetUnauthorised()
    {
        return Unauthorized();
    }
    [HttpGet("validation-error")]
    public ActionResult GetValidationError()
    {
        ModelState.AddModelError("Probleme 1", "Error 1");
        ModelState.AddModelError("Probleme 2", "Error 2");
        ModelState.AddModelError("Probleme 3", "Error 3");
        ModelState.AddModelError("Probleme 4", "Error 4");
        return ValidationProblem();
    }
    [HttpGet("server-error")]
    public ActionResult GetServerError()
    {
        throw new Exception("This is a Server Error");
    }
}
