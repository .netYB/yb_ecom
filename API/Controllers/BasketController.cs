using API.Data;
using API.DTO;
using API.Entities;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers;

[ApiController]
[Route("api/[controller]")]
public class BasketController : BaseApiController
{
    private readonly IMapper _mapper;
    private readonly StoreContext _context;
    public BasketController(StoreContext context, IMapper mapper)
    {
        _context = context;
        _mapper = mapper;
    }
    [HttpGet(Name ="GetBasket")]
    public async Task<ActionResult<BasketDto>> GetBasket()
    {
        var basket = await RetrieveBasket();
        if (basket == null) return NotFound();
        var basketDto = _mapper.Map<BasketDto>(basket);
        return basketDto;
    }

    [HttpPost(Name ="AddUpdateItemBasket")]
    public async Task<ActionResult<BasketDto>> AddItemBasket(int productId, int quantity)
    {
        // Get Basket
        var basket = await RetrieveBasket();
        // Create Basket 
        if (basket == null) basket = CreateBasket();
        // Get Product
        var product = await _context.Products.FindAsync(productId);
        if (product == null) return BadRequest(new ProblemDetails { Title = "Product not found"});
        // Add item
        basket.AddItem(product, quantity);
        // Save change
        var result = await _context.SaveChangesAsync() > 0;
        var basketDto = _mapper.Map<BasketDto>(basket);
        if (result) return CreatedAtRoute("GetBasket",basketDto);
        return BadRequest(new ProblemDetails { Title = "Probleme saving bascket item"});
    }

    [HttpDelete(Name ="DeleteBasket")]

    public async Task<ActionResult> RemoveItemBasket(int productId, int quantity)
    {
        // Get Basket
        var basket = await RetrieveBasket();
        // Get Product
        var product = await _context.Products.FindAsync(productId);
        if (product == null) return BadRequest(new ProblemDetails { Title = "Product not found to remove"});
        // Remove or reduce quantity
        basket.RemoveItem(product, quantity);
        // Save change
        var result = await _context.SaveChangesAsync() > 0;
        return Ok();
    }


    // Methodes 
    private async Task<Basket> RetrieveBasket()
    {
        return await _context.Baskets
        .Include(i => i.Items)
        .ThenInclude(p => p.Product)
        .FirstOrDefaultAsync(b => b.BuyerId == Request.Cookies["buyerId"]);
    }

    private Basket CreateBasket()
    {
        var buyerId = Guid.NewGuid().ToString();
        var cookieOption = new CookieOptions
        {
            IsEssential = true,
            Expires = DateTime.Now.AddDays(30)
        };
        Response.Cookies.Append("buyerId", buyerId, cookieOption);
        var basket = new Basket { BuyerId = buyerId };
        _context.Baskets.Add(basket);
        return basket;
    }
}
