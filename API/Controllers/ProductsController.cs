using System.Text.Json;
using API.Data;
using API.Entities;
using API.Extensions;
using API.RequestHelpers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace API.Controllers
{
    public class ProductsController : BaseApiController
    {
        private readonly StoreContext _context;
        public ProductsController(StoreContext context)
        {
            _context = context;

        }
        [HttpGet]
        // A la place de pagedList on avait just List ! 
        public async Task<ActionResult<PagedList<Product>>> GetProducts([FromQuery]
             ProductParmas productParms)
        {
            var query = _context.Products
            .Sort(productParms.orderBy)
            .Search(productParms.searchTerm)
            .Filter(productParms.brands, productParms.types)
            .AsQueryable();
            var products = await PagedList<Product>.ToPagedList(query, productParms.pageNumber, productParms.PageSize);
            Response.AddPaginationHeader(products.MetaData);
            return Ok(products);
        }
        
        [HttpGet("{id}")]
        public async Task<ActionResult<Product>> GetProduct(int id)
        {
            var product = await _context.Products.FindAsync(id);
            if (product == null) return NotFound();
            return Ok(product);
        }

        // Créer les filters du brand et types
        [HttpGet("filters")]
        public async Task<ActionResult> GetFilters()
        {
            var brands = await _context.Products.Select(p => p.Brand).Distinct().ToListAsync();
            var types = await _context.Products.Select(p => p.Type).Distinct().ToListAsync();

            // Anynomous Object
            return Ok(new { brands, types });
        }
    }
}