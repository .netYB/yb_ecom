import TestHook from "../../hooks/TestHook";

const HomePage = () => {
  return (
    <>
      <TestHook/>
      <div>HomePage</div>
    </>
  );
};

export default HomePage;
