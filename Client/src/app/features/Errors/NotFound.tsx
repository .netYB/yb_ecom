import { Alert, AlertTitle, List, ListItem } from "@mui/material";
import { useLocation } from "react-router-dom";

const NotFound = () => {
  const { state } = useLocation();
  console.log(state);

  return (
    <>
      <div>NotFound</div>
      {state &&  <Alert severity="error">
          <AlertTitle>
            Unique error
          </AlertTitle>
          <List>
              <ListItem>Status : {state.status}</ListItem>
              <ListItem>Title : {state.title}</ListItem>
              <ListItem>{state.detail}</ListItem>
          </List>
        </Alert>}
    </>
  );
};

export default NotFound;
