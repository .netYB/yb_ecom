import { Alert, AlertTitle, Button, ButtonGroup, List, ListItem, Typography } from "@mui/material";
import { useEffect, useState } from "react";
import agent from "../../api/agent";

const ErrorsPage = () => {
  const [validationError, setValidationError] = useState([]);
  
  useEffect(() => {
    console.log(validationError);
 
  }, [validationError])
  
  const HandleValidation = () => {
    agent.TestErrors.getValidationError()
      .then((_e) => console.log("impossible de voir ce message"))
      .catch((errors) => setValidationError(errors));
  };

  return (
    <>
      <Typography gutterBottom variant="h2">
        Error Testing purpose !
      </Typography>
      <ButtonGroup fullWidth>
        <Button
          onClick={() =>
            agent.TestErrors.get400Error().catch((error) => {console.log(error);setValidationError([])})
          }
        >
          400 Error
        </Button>
        <Button
          onClick={() =>
            agent.TestErrors.get401Error().catch((error) => console.log(error))
          }
        >
          401 Error
        </Button>
        <Button
          onClick={() =>
            agent.TestErrors.get404Error().catch((error) => console.log(error))
          }
        >
          404 Error
        </Button>
        <Button
          onClick={() =>
            agent.TestErrors.get500Error().catch((error) => console.log(error))
          }
        >
          500 Error
        </Button>
        <Button onClick={()=>HandleValidation()}>Validation Error</Button>
      </ButtonGroup>
      {
        validationError.length>0 && <Alert severity="error">
          <AlertTitle>
            Validation Errors
          </AlertTitle>
          <List>
            {validationError.map(error=>
              <ListItem key={error}>{error}</ListItem>
              )}
          </List>
        </Alert>
      }
    </>
  );
};

export default ErrorsPage;
