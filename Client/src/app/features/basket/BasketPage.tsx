import { Add, Delete, Remove } from "@mui/icons-material";
import { LoadingButton } from "@mui/lab";
import {
  TableContainer,
  Paper,
  Table,
  TableHead,
  TableRow,
  TableCell,
  TableBody,
  Box,
  Grid,
  Button,
} from "@mui/material";
import { useState } from "react";
import { Link, NavLink } from "react-router-dom";
import agent from "../../api/agent";
import {
  addBasketItemAsync,
  removeBasketItemAsync,
  setBasket,
} from "../../context/Slices/basketSlice";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../context/StoreReduxConfig";
import BasketSummary from "./BasketSummary";

const BasketPage = () => {
  const dispatch = useTypeDispatch();
  const { basket, status } = useTypeSelector((state) => state.basket);

  return (
    <>
      <TableContainer component={Paper}>
        <Table sx={{ minWidth: 900 }} aria-label="simple table">
          <TableHead>
            <TableRow>
              <TableCell>Product ID</TableCell>
              <TableCell align="left">Name</TableCell>
              <TableCell align="left">Type</TableCell>
              <TableCell align="center">Quantity</TableCell>
              <TableCell align="left">Brand</TableCell>
              <TableCell align="right">Unit Price</TableCell>
              <TableCell align="right">Price totale</TableCell>
              <TableCell align="right"></TableCell>
            </TableRow>
          </TableHead>
          <TableBody>
            {basket?.items.map((item) => (
              <TableRow
                key={item.name}
                sx={{ "&:last-child td, &:last-child th": { border: 0 } }}
              >
                <TableCell component="th" scope="row">
                  <Box
                    component={NavLink}
                    to={`/catalog/${item.productId}`}
                    display="flex"
                    alignItems={"center"}
                  >
                    <img
                      src={item.pictureUrl}
                      alt={item.name}
                      style={{ height: 50 }}
                    ></img>
                    <span>{item.name}</span>
                  </Box>
                </TableCell>
                <TableCell align="left">{item.name}</TableCell>
                <TableCell align="left">{item.type}</TableCell>
                <TableCell align="center">
                  <LoadingButton
                    size="small"
                    loading={status === "pendingRemoveItem" + item.productId +'rem'}
                    onClick={() =>
                      dispatch(
                        removeBasketItemAsync({
                          productId: item.productId,
                          quantity: 1,
                          name:'rem'
                        })
                      )
                    }
                  >
                    <Remove></Remove>
                  </LoadingButton>

                  {item.quantity}
                  <LoadingButton
                    size="small"
                    loading={status === "pendingAddItem" + item.productId}
                    onClick={() =>
                      dispatch(
                        addBasketItemAsync({ productId: item.productId })
                      )
                    }
                  >
                    <Add></Add>
                  </LoadingButton>
                </TableCell>
                <TableCell align="left">{item.brand}</TableCell>
                <TableCell align="right">
                  $ {(item.price / 100).toFixed(2)}
                </TableCell>
                <TableCell align="right">
                  $ {((item.price / 100) * item.quantity).toFixed(2)}
                </TableCell>
                <TableCell align="center">
                  <LoadingButton
                    color="error"
                    loading={
                      status === "pendingRemoveItem" + item.productId + 'del'
                    }
                    onClick={() =>
                      dispatch(
                        removeBasketItemAsync({
                          productId: item.productId,
                          quantity: item.quantity,
                          name: "del",
                        })
                      )
                    }
                  >
                    <Delete />
                  </LoadingButton>
                </TableCell>
              </TableRow>
            ))}
          </TableBody>
        </Table>
      </TableContainer>
      <Grid container sx={{ mb: 12 }}>
        <Grid item xs={6}></Grid>
        <Grid item xs={6}>
          <BasketSummary />
          <Grid container>
            <Grid xs={6} item></Grid>
            <Grid xs={6} item>
              <Box component={Link} to="/checkout">
                <Button fullWidth variant="outlined" sx={{ mt: 4 }}>
                  Checkout
                </Button>
              </Box>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </>
  );
};

export default BasketPage;
