import { debounce, TextField } from "@mui/material";
import { useState } from "react";
import { setProductParams } from "../../context/Slices/catalogSlice";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../context/StoreReduxConfig";

const ProductSearch = () => {
  const { productParams } = useTypeSelector((state) => state.catalog);
  const [searchTerm, setSearchTerm] = useState(productParams.searchTerm);
  const dispatch = useTypeDispatch();

  // Add function to delay search while user is typing Serach term
  const debouncedSearch = debounce((event: any) => {
    dispatch(setProductParams({ searchTerm: event.target.value }));
  },3000);

  return (
    <TextField
      label="Search product ..."
      variant="outlined"
      fullWidth
      value={searchTerm || ""}
      onChange={(event) => {
        setSearchTerm(event.target.value);
        debouncedSearch(event);
      }}
    ></TextField>
  );
};

export default ProductSearch;
