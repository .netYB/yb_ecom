import { Grid, Skeleton } from "@mui/material";
import { Box } from "@mui/system";
import ProductCardSkeleton from "../../components/Skeleton/ProductCardSkeleton";
import { useTypeSelector } from "../../context/StoreReduxConfig";
import { Product } from "../../models/product";
import ProductCart from "./ProductCart";

interface Props {
  products: Product[];
}

const ProductList = ({ products }: Props) => {
  const { productLoaded } = useTypeSelector((state) => state.catalog);
  return (
    <Grid container spacing={4} sx={{ mb: 3 }}>
      {products.map((product) => {
        return productLoaded ? (
          <Grid key={product.id} item xs={4}>
            <ProductCart product={product} />
          </Grid>
        ) : (
          <Grid key={product.id} item xs={4}>
            <ProductCardSkeleton />
          </Grid>
        );
      })}
    </Grid>
  );
};

export default ProductList;
