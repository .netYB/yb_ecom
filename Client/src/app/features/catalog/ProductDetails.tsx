import { LoadingButton } from "@mui/lab";
import {
  Divider,
  Grid,
  Table,
  TableBody,
  TableCell,
  TableContainer,
  TableRow,
  TextField,
  Typography,
} from "@mui/material";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";

import {
  addBasketItemAsync,
  removeBasketItemAsync
} from "../../context/Slices/basketSlice";
import { fetchProductAsync, productSelectors } from "../../context/Slices/catalogSlice";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../context/StoreReduxConfig";
import Loading from "../../layout/Loading";
import { BasketItem } from "../../models/basket";

const ProductDetails = () => {
  const dispatch = useTypeDispatch();

  const [quantity, setQuantity] = useState(0);

  const { id } = useParams<{ id: string }>();
  const { basket,status } = useTypeSelector((state) => state.basket);
  const { status : statusProduct } = useTypeSelector((state) => state.catalog);
  const product = useTypeSelector(state =>productSelectors.selectById(state,id!))
  

  
  const item = basket?.items.find(
    (i: BasketItem) => i.productId === product?.id
  );

  const handleInputChange = (event: any) => {
    if (event.target.value > 0) {
      setQuantity(parseInt(event.target.value));
    }
  };

  const handleUpdateCart = (event: any) => {
    if (!item || quantity > item.quantity) {
      const updateQuantity = item ? quantity - item.quantity : quantity;
      console.log(status);
      
      dispatch(
        addBasketItemAsync({
          productId: product?.id!,
          quantity: updateQuantity,
        })
      );
    } else {
      const updateQuantity = item.quantity - quantity;
      dispatch(
        removeBasketItemAsync({
          productId: product?.id!,
          quantity: updateQuantity,
        })
      );
    }
  };

  useEffect(() => {
    if (item) {
      setQuantity(item.quantity);
    }
    if(!product && id) dispatch(fetchProductAsync(parseInt(id)))
  }, [id, item, product, dispatch]);

  if (statusProduct ==='pendingFetchProduct') return <Loading />;

  if (!product) return <div>Not found !</div>;

  return (
    <div>
      <Grid container spacing={6}>
        <Grid item xs={6}>
          <img src={product.pictureUrl} />
        </Grid>
        <Grid item xs={6}>
          <Typography variant="h4">{product.brand}</Typography>
          <Divider sx={{ my: 2 }} />
          <Typography variant="h4">{product.name}</Typography>
          <Divider sx={{ my: 2 }} />
          <Typography variant="h5" color="secondary">
            ${product.price}
          </Typography>
          <TableContainer>
            <Table>
              <TableBody>
                <TableRow>
                  <TableCell>Name</TableCell>
                  <TableCell>{product.name}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Description</TableCell>
                  <TableCell>{product.description}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Type</TableCell>
                  <TableCell>{product.type}</TableCell>
                </TableRow>
                <TableRow>
                  <TableCell>Stock </TableCell>
                  <TableCell>{product.quantityInStock}</TableCell>
                </TableRow>
              </TableBody>
            </Table>
          </TableContainer>
          <Grid container spacing={2} sx={{ mt: 4 }}>
            <Grid item xs={6}>
              <TextField
                label="Quatity in cart"
                variant="outlined"
                type={"number"}
                fullWidth
                value={quantity}
                onChange={(event) => handleInputChange(event)}
              ></TextField>
            </Grid>
            <Grid item xs={6}>
              <LoadingButton
                loading={status.includes("pending") }
                variant="contained"
                color="primary"
                size="large"
                fullWidth
                sx={{ height: 55 }}
                onClick={handleUpdateCart}
                disabled={
                  (!item && quantity == 0) || item?.quantity == quantity
                }
              >
                {item ? "Update quatity" : "Add to Cart"}
              </LoadingButton>
            </Grid>
          </Grid>
        </Grid>
      </Grid>
    </div>
  );
};

export default ProductDetails;
