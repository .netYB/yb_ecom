import LoadingButton from "@mui/lab/LoadingButton";
import {
  Avatar,
  Button,
  Card,
  CardActions,
  CardContent,
  CardHeader,
  CardMedia,
  Typography,
} from "@mui/material";
import { Link } from "react-router-dom";
import { addBasketItemAsync } from "../../context/Slices/basketSlice";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../context/StoreReduxConfig";
import { Product } from "../../models/product";

interface Props {
  product: Product;
}

const ProductCart = ({ product }: Props) => {
  const dispatch = useTypeDispatch();
  const { status } = useTypeSelector((state) => state.basket);

  return (
    <Card sx={{ maxWidth: 345 }}>
      <CardHeader
        avatar={
          <Avatar >
            {product.name.charAt(0).toUpperCase()}
          </Avatar>
        }
        title={product.name}
        titleTypographyProps={{
          sx: { fontWeight: "bold" },
          color: "primary.main",
        }}
      />
      <CardMedia
        sx={{ height: 200, backgroundSize: "contain", bgcolor: "primary.main" }}
        image={product.pictureUrl}
        title={product.name}
      />
      <CardContent>
        <Typography gutterBottom variant="h5" component="div">
          {product.name}
        </Typography>
        <Typography gutterBottom color="secondary" variant="h5" component="div">
          $ {(product.price / 100).toFixed(2)}
        </Typography>
        <Typography variant="body2" color="text.secondary">
          {product.type} / {product.brand}
        </Typography>
      </CardContent>
      <CardActions>
        <LoadingButton
          loading={status==="pendingAddItem"+product.id}
          onClick={() =>
            dispatch(addBasketItemAsync({ productId: product.id }))
          }
          size="small"
        >
          Add to cart
        </LoadingButton>
        <Button component={Link} to={`/catalog/${product.id}`} size="small">
          View
        </Button>
      </CardActions>
    </Card>
  );
};

export default ProductCart;
