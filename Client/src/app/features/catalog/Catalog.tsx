import { Grid, Paper } from "@mui/material";
import { useEffect } from "react";
import { CheckBoxGroup } from "../../components/CheckBox/CheckBoxGroup";
import PaginationGroup from "../../components/Pagination/PaginationGroup";
import RadioButtonGroup from "../../components/RadioButton/RadioButtonGroup";
import {
  fetchAllProductsAsync,
  fetchFiltersAsync,
  productSelectors,
  setPageNumber,
  setProductParams,
} from "../../context/Slices/catalogSlice";
import {
  useTypeDispatch,
  useTypeSelector,
} from "../../context/StoreReduxConfig";
import ProductList from "./ProductList";
import ProductSearch from "./ProductSearch";

const sortOptions = [
  { value: "name", label: "Name" },
  { value: "priceDesc", label: "Price - Hight to low" },
  { value: "price", label: "Price - Low to hight" },
];

const Catalog = () => {
  const {
    filtersLoaded,
    productLoaded,
    brands,
    types,
    productParams,
    metaData,
  } = useTypeSelector((state) => state.catalog);
  const dispatch = useTypeDispatch();
  const products = useTypeSelector(productSelectors.selectAll);

  useEffect(() => {
    if (!productLoaded) dispatch(fetchAllProductsAsync());
  }, [productLoaded, dispatch]);

  useEffect(() => {
    if (!filtersLoaded) dispatch(fetchFiltersAsync());
  }, [filtersLoaded, dispatch]);

  return (
    <>
      <Grid container columnSpacing={4} sx={{ mb: 2 }}>
        <Grid item xs={3}>
          <Paper sx={{ mb: 2 }}>
            <ProductSearch />
          </Paper>
          <Paper sx={{ mb: 2, p: 2 }}>
            <RadioButtonGroup
              options={sortOptions}
              onChange={(event) =>
                dispatch(setProductParams({ orderBy: event.target.value }))
              }
              selectedValue={productParams.orderBy}
            />
          </Paper>
          <Paper sx={{ mb: 2, p: 2 }}>
            <CheckBoxGroup
              filtersLoaded={filtersLoaded}
              checked={productParams.brands}
              items={brands}
              onChange={(items: string[]) =>
                dispatch(setProductParams({ brands: items }))
              }
            />
          </Paper>
          <Paper sx={{ mb: 2, p: 2 }}>
            <CheckBoxGroup
              filtersLoaded={filtersLoaded}
              checked={productParams.types}
              items={types}
              onChange={(items: string[]) =>
                dispatch(setProductParams({ types: items }))
              }
            />
          </Paper>
        </Grid>
        <Grid item xs={9}>
          <ProductList products={products} />
        </Grid>
      </Grid>
      <Grid container sx={{ mb: 2 }}>
        <Grid item xs={3}></Grid>
        <Grid item xs={9}>
          {metaData && (
            <PaginationGroup
              metaData={metaData}
              onPageChange={(page: number) => {
                dispatch(setPageNumber({ pageNumber: page }));
              }}
            />
          )}
        </Grid>
      </Grid>
    </>
  );
};

export default Catalog;
