import { green, grey, lightBlue, orange, red, yellow } from '@mui/material/colors';
import { createTheme } from '@mui/material/styles';

const themeb = createTheme({
  palette: {
    mode: 'dark',
    primary: {
      light: '#A5D7E8',
      main: "#576CBC"
    },
    // secondary: {
    //   light: red[500],
    //   main: red[700],
    //   dark: red[900],
    //   contrastText: grey[50]
    // },
    // error: {
    //   light: red[400],
    //   main: red[500],
    //   dark: red[300],
    //   contrastText: grey[800]
    // },
    // success: {
    //   main: green[500]
    // },
    // warning: {
    //   main: yellow[500],
    //   contrastText: grey[800]
    // },
    // info: {
    //   main: lightBlue[500]
    // },
    // text: {
    //   primary: grey[900],
    //   secondary: grey[700],
    //   disabled: grey[500]
    // },
    // action: {
    //   active: red[200],
    //   disabled: grey[700],
    //   disabledBackground: grey[200],
    //   hover: red[100],
    //   hoverOpacity: 0.7,
    //   focus: red[600],
    //   focusOpacity: 1,
    //   selected: red[300],
    //   selectedOpacity: 1
    // },
    background: {
      default: "#303030",
      paper: "#424242"
    },
    // common: {
    //   black: grey[900],
    //   white: grey[200]
    // },
    // tonalOffset: 0.2
  }
});

export default themeb;