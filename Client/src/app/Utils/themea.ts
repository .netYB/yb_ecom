import { createTheme } from '@mui/material/styles';

const themea = createTheme({
  palette: {
    mode: 'light',
    primary: {
      light: "#064663",
      main: "#04293A"
    },
    
    background: {
      default: "#fafafa",
      paper: "#fff"
    },
  },
});

export default themea;

// https://mui.com/material-ui/customization/theming/
// https://networksynapse.net/development/mui-v5-themes/#comment-832
// https://bareynol.github.io/mui-theme-creator/