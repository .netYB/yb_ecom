export function getCookie(key: string) {
    const cookie = document.cookie.match("(^|;)\\s*" + key + "\\s*([^;]+)");
    return cookie ? cookie.pop() : ''
}