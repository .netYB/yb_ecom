import { createBrowserRouter, Navigate } from "react-router-dom";
import AboutPage from "../features/about/AboutPage";
import BasketPage from "../features/basket/BasketPage";
import Catalog from "../features/catalog/Catalog";
import ProductDetails from "../features/catalog/ProductDetails";
import CheckoutPage from "../features/checkout/CheckoutPage";
import ContactPage from "../features/contact/ContactPage";
import ErrorsPage from "../features/errors/ErrorsPage";
import NotFound from "../features/errors/NotFound";
import HomePage from "../features/home/HomePage";
import App from "../layout/App";

export const router = createBrowserRouter([
  {
    path: "/",
    element: <App />,
    children: [
      { path: "", element: <HomePage /> },
      { path: "catalog", element: <Catalog /> },
      { path: "catalog/:id", element: <ProductDetails /> },
      { path: "basket", element: <BasketPage /> },
      { path: "about", element: <AboutPage /> },
      { path: "contact", element: <ContactPage /> },
      { path: "checkout", element: <CheckoutPage /> },
      { path: "Error", element: <ErrorsPage /> },
      { path: "404", element: <NotFound /> },
      { path: "/login", element: <CheckoutPage /> },
      { path: "*", element: <Navigate replace to={"404"} /> },
    ],
  },
]);
