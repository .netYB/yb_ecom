import { createAsyncThunk, createEntityAdapter, createSlice } from "@reduxjs/toolkit";
import agent from "../../api/agent";
import { MetaData } from "../../models/pagination";
import { Product, ProductParms } from "../../models/product";
import { RootState } from "../StoreReduxConfig";

interface CatalogState {
    productLoaded: boolean,
    filtersLoaded: boolean,
    status: string,
    brands: [],
    types: [],
    productParams: ProductParms,
    metaData: MetaData | null
}

//Create a build in EntityAdapter qui va nous generer nos entity ainsi une initial state par default et avec des selectors predefini 
const productAdapter = createEntityAdapter<Product>()


// Function passing UrlParms
const getAxiosParams = (productParam: ProductParms) => {
    const params = new URLSearchParams();
    if (productParam.searchTerm) params.append('searchTerm', productParam.searchTerm)
    params.append('orderBy', productParam.orderBy)
    params.append('pageNumber', productParam.pageNumber.toString())
    params.append('pageSize', productParam.pageSize.toString())
    if (productParam.brands.length>0) params.append('brands', productParam.brands.toString())
    if (productParam.types.length>0) params.append('types', productParam.types.toString())
    return params
}
// Asyc function thunk pour fetch tout les donnees
export const fetchAllProductsAsync = createAsyncThunk<Product[], void, { state: RootState }>(
    'Product/fetchAllsAsync',
    async (_, thunkAPI) => {
        try {
            const params = getAxiosParams(thunkAPI.getState().catalog.productParams)
            const response = await agent.Catalog.list(params)
            thunkAPI.dispatch(setMetaData(response.metaData))
            return response.items
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)

// Asyc function thunk pour fetch tout les filter disponible
export const fetchFiltersAsync = createAsyncThunk(
    'Product/fetchfiltersAsync',
    async (_, thunkAPI) => {
        try {
            return await agent.Catalog.fetchFilters()
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)

// Asyc function thunk pour fetch une donnees
export const fetchProductAsync = createAsyncThunk<Product, number>(
    'Product/fetchsOneAsync',
    async (id: number, thunkAPI) => {
        try {
            return await agent.Catalog.details(id)
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)
// Creer Slice avec initial state d'adapter
export const catalogSlice = createSlice({
    name: 'Product',
    initialState: productAdapter.getInitialState<CatalogState>({
        productLoaded: false,
        filtersLoaded: false,
        status: "idle",
        brands: [],
        types: [],
        productParams: {
            orderBy: 'name',
            searchTerm: '',
            brands: [],
            types: [],
            pageNumber: 1,
            pageSize: 9
        },
        metaData: null
    }),
    reducers: {
        setProductParams: (state, action) => {
            state.productLoaded = false
            // ajoutin page number to 1 pour reste la page lors d'un filtre
            state.productParams = { ...state.productParams, ...action.payload, pageNumber:1 }
        },
        setMetaData: (state, action) => {
            state.metaData = action.payload
        },
        setPageNumber: (state, action) => {
            state.productLoaded = false;
            state.productParams = { ...state.productParams, ...action.payload }
        },
        resetProductParams: (state) => {
            state.productLoaded = false
            state.productParams = {
                orderBy: 'name',
                searchTerm: '',
                brands: [],
                types: [],
                pageNumber: 1,
                pageSize: 6
            }
        }
    },
    extraReducers: (builder => {
        //
        // Get All Data Reducers
        // 

        // 1 - Loading data
        builder.addCase(fetchAllProductsAsync.pending, (state) => {

            state.status = 'pendingFetchAllProduct';
        })
        // 2 - Stocker les donnees
        builder.addCase(fetchAllProductsAsync.fulfilled, (state, action) => {
            state.productLoaded = true
            productAdapter.setAll(state, action.payload)
            state.status = 'idle'
        })
        // 3 - En cas d'erreur
        builder.addCase(fetchAllProductsAsync.rejected, (state, action) => {
            console.log(action.payload);
            state.status = 'idle';
        })

        //
        // Get One Data Reducers
        //

        // 1 - Loading
        builder.addCase(fetchProductAsync.pending, (state) => {
            state.status = 'pendingFetchProduct';
        })
        // 2 - Stocker les donnees
        builder.addCase(fetchProductAsync.fulfilled, (state, action) => {
            productAdapter.upsertOne(state, action.payload)
            state.status = 'idle'
        })

        // 3 - En cas d'erreur
        builder.addCase(fetchProductAsync.rejected, (state, action) => {
            console.log(action.payload);
            state.status = 'idle';
        })

        // 
        // Get All Filters
        // 

        // 1 - Loading
        builder.addCase(fetchFiltersAsync.pending, (state) => {
            state.status = 'pendingFetchFilter';
        })

        // 2 - Stocker les donnees
        builder.addCase(fetchFiltersAsync.fulfilled, (state, action) => {
            state.brands = action.payload.brands
            state.types = action.payload.types
            state.filtersLoaded = true
            state.status = 'idle'
        })

        // 3- En cas d'erreur
        builder.addCase(fetchFiltersAsync.rejected, (state, action) => {
            console.log(action.payload)
            state.status = 'idle'
        })
    })
}
)


// Export les selectors predefinie d'adapter
export const { setProductParams, resetProductParams, setMetaData, setPageNumber } = catalogSlice.actions;

// Export les selectors predefinie d'adapter
export const productSelectors = productAdapter.getSelectors((state: RootState) => state.catalog)