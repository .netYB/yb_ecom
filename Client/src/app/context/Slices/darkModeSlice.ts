import { createSlice } from "@reduxjs/toolkit";

interface DarkState {
    darkMode: boolean
    systemPref: boolean
}
const initialState: DarkState = {
    darkMode: false,
    systemPref: false
}

export const darkmodeSlice = createSlice({
    name: "DarkMode",
    initialState,
    reducers: {
        setDarkMode: (state) => {
            // Ajouter DarkMode
            state.darkMode = !state.darkMode;
            state.systemPref = false
        },
        setDefaultTheme: (state) => {
            // defaultTheme
            const matches = window.matchMedia('(prefers-color-scheme: dark)').matches;
            if (matches) {
                state.darkMode = true
            } else {
                state.darkMode = false
            }
        }
    },

})

export const { setDarkMode, setDefaultTheme } = darkmodeSlice.actions;