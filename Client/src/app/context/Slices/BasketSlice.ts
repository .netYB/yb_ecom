import { createAsyncThunk, createSlice } from "@reduxjs/toolkit";
import agent from "../../api/agent";
import { Basket } from "../../models/basket";

interface BasketState {
    basket: Basket | null;
    status: string
}
const initialState: BasketState = {
    basket: null,
    status: 'repos'
}

export const addBasketItemAsync = createAsyncThunk<Basket, { productId: number, quantity?: number }>(
    'basket/addBasketItemAsync',
    async ({ productId, quantity = 1 }, thunkAPI) => {
        try {
            return await agent.Basket.addItem(productId, quantity)
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)
export const removeBasketItemAsync = createAsyncThunk<void, { productId: number, quantity: number, name?: string }>(
    'basket/removeBasketItemAsync',
    async ({ productId, quantity }, thunkAPI) => {
        try {
            return await agent.Basket.removeItem(productId, quantity)
        } catch (error: any) {
            return thunkAPI.rejectWithValue({ error: error })
        }
    }
)

export const basketSlice = createSlice({
    name: "basket",
    initialState,
    reducers: {
        setBasket: (state, action) => {
            // Ajouter basket
            state.basket = action.payload
        }
    },
    // Pour les Call API
    extraReducers: (builder) => {
        // Add Item
        builder.addCase(addBasketItemAsync.pending, (state, action) => {
            state.status = 'pendingAddItem' + action.meta.arg.productId
        })
        builder.addCase(addBasketItemAsync.fulfilled, (state, action) => {
            state.basket = action.payload
            state.status = 'repos'
        })
        builder.addCase(addBasketItemAsync.rejected, (state,action) => {
            console.log(action.payload);
            state.status = 'repos'
        })
        // Delete Item
        builder.addCase(removeBasketItemAsync.pending, (state, action) => {
            state.status = 'pendingRemoveItem' + action.meta.arg.productId + action.meta.arg.name
        })
        builder.addCase(removeBasketItemAsync.fulfilled, (state, action) => {
            // extraire l'ID et Quantity du action meta 
            const { productId, quantity } = action.meta.arg;
            // Chercher l'items avec ce ID du payload // reurn -1 if not found
            const itemIndex = state.basket?.items.findIndex((item) => item.productId === productId);
            // Si l'item n'est pas trouver // Pour defense on verifie undefined aussi
            if (itemIndex == -1 || itemIndex == undefined) return;
            // Diminuer la quantiter de l'item
            state.basket!.items[itemIndex].quantity -= quantity;
            // Si on atteint quantite 0 on supprime l'item
            if (state.basket?.items[itemIndex].quantity == 0) state.basket.items.splice(itemIndex, 1);
            state.status = 'repos'
        })
        builder.addCase(removeBasketItemAsync.rejected, (state,action) => {
            console.log(action.payload);
            state.status = 'repos'
        })
    }
})

export const { setBasket } = basketSlice.actions;