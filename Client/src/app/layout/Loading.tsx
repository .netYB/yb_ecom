import { Backdrop, Paper, useTheme } from "@mui/material";
import { Box } from "@mui/system";

interface Props {
  message?: string;
}

const Loading = ({ message = "Loading..." }: Props) => {
  return (
    <Paper>
      <Backdrop open={true} invisible={true} sx={{ zIndex: 10 }}>
        <Box
          sx={{ width: "100%", height: "600px", zIndex: 15 }}
          display={"flex"}
          alignItems={"center"}
          justifyContent={"center"}
        >
          <Box
            display={"flex"}
            alignItems={"center"}
            justifyContent={"center"}
            gap={4}
            position={"relative"}
            width={500}
            height={500}
            sx={{ marginTop: "100px" }}
          >
            <Box
              position={"absolute"}
              width={200}
              height={200}
              marginTop={0}
              sx={{
                backgroundColor: '#808080',
                borderRadius: "50%",
                animation: "ping 1.5s cubic-bezier(0, 0, 0.2, 1) infinite",
                animationDelay: "-150ms",
              }}
            />
            <Box
              position={"absolute"}
              width={110}
              height={110}
              marginTop={0}
              sx={{
                backgroundColor: '#808080',
                borderRadius: "50%",
                animation: " ping 1.5s cubic-bezier(0, 0, 0.2, 1) infinite",
                animationDelay: "-100ms",
              }}
            />
            <Box
              position={"absolute"}
              width={55}
              height={55}
              marginTop={0}
              sx={{
                backgroundColor: '#808080',
                borderRadius: "50%",
                animation: "ping 1.5s cubic-bezier(0, 0, 0.2, 1) infinite",
                animationDelay: "0ms",
              }}
            />
          </Box>
        </Box>
      </Backdrop>
    </Paper>
  );
};

export default Loading;
