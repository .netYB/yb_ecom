import { alpha, AppBar, AppBarProps } from '@mui/material';
import { styled } from '@mui/material/styles';

export const AppBarYB = styled(AppBar)<AppBarProps>(({ theme }) => ({
   backgroundColor: alpha(theme.palette.mode === "dark" ? "#808080" : theme.palette.primary.light, 0.6),
   boxShadow: `0 4px 30px rgba(0, 0, 0, 0.1)`,
   backdropFilter: `blur(10px)`,
}));