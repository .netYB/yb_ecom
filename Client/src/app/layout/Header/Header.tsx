import * as React from "react";
import AppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import IconButton from "@mui/material/IconButton";
import Typography from "@mui/material/Typography";
import Menu from "@mui/material/Menu";
import MenuIcon from "@mui/icons-material/Menu";
import Container from "@mui/material/Container";
import AdbIcon from "@mui/icons-material/Adb";
import {
  Switch,
  MenuItem,
  Box,
  ListItem,
  List,
  Badge,
  Stack,
  Button,
} from "@mui/material";
import { NavLink } from "react-router-dom";
import { ShoppingBag } from "@mui/icons-material";
import { useTypeDispatch, useTypeSelector } from "../../context/StoreReduxConfig";
import { setDarkMode } from "../../context/Slices/darkModeSlice";
import { AppBarYB } from "./HeaderYB";
import HideOnScroll from "./HideOnScroll";
import { InwiLogoIcon } from "../../../assets/icons/inwi-logo";
import YBLogo from "../../../assets/icons/YBLogo";



const pages = [
  { title: "Catalog", path: "/catalog" },
  { title: "Contact", path: "/contact" },
  { title: "About", path: "/about" },
  { title: "Error", path: "/error" },
];

const login = [
  { title: "Login", path: "/login" },
  { title: "Register", path: "/register" },
];

function Header() {
  const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(
    null
  );

  const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
    setAnchorElNav(event.currentTarget);
  };

  const handleCloseNavMenu = () => {
    setAnchorElNav(null);
  };

  const {basket} = useTypeSelector(state=> state.basket);
  const dispatch = useTypeDispatch();

  const countItems = basket?.items.reduce((sum,item)=>sum + item.quantity,0)
  return (
    <HideOnScroll >
    <AppBarYB >
      <Container maxWidth="xl">
        <Toolbar  disableGutters sx={{height:`64px`}}>
          {/* Start Main icone + textLogo */}
          <YBLogo className="w-10 h-10 mr-1 md:flex xs:hidden"/>
          {/* End Main icone + textLogo */}
          {/* Start Responsive icone + textLogo */}
          <Box sx={{ flexGrow: 1, display: { xs: "flex", md: "none" } }}>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleOpenNavMenu}
              color="inherit"
            >
              <MenuIcon />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorElNav}
              anchorOrigin={{
                vertical: "bottom",
                horizontal: "left",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "left",
              }}
              open={Boolean(anchorElNav)}
              onClose={handleCloseNavMenu}
              sx={{
                display: { xs: "block", md: "none" },
              }}
            >
              {pages.map((page) => (
                <MenuItem key={page.path} onClick={handleCloseNavMenu}>
                  <Typography
                    component={NavLink}
                    to={page.path}
                    textAlign="center"
                    sx={{
                      "&:hover": {
                        color: "red",
                      },
                      "&.active": {
                        color: "green",
                      },
                    }}
                  >
                    {page.title}
                  </Typography>
                </MenuItem>
              ))}
            </Menu>
          </Box>
          <YBLogo className="mr-1 md:hidden xs:flex"/>

          {/* Link */}
          <Stack direction="row" spacing={2}>
            <List sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {pages.map((page) => (
                <ListItem
                  component={NavLink}
                  to={page.path}
                  key={page.path}
                  onClick={handleCloseNavMenu}
                  sx={{
                    my: 2,
                    color: "white",
                    display: "block",
                    "&:hover": {
                      color: "red",
                    },
                    "&.active": {
                      color: "green",
                    },
                  }}
                >
                  {page.title}
                </ListItem>
              ))}
            </List>
            <List sx={{ flexGrow: 1, display: { xs: "none", md: "flex" } }}>
              {login.map((page) => (
                <ListItem
                  component={NavLink}
                  to={page.path}
                  key={page.path}
                  onClick={handleCloseNavMenu}
                  sx={{
                    my: 2,
                    color: "white",
                    display: "block",
                    "&:hover": {
                      color: "red",
                    },
                    "&.active": {
                      color: "green",
                    },
                  }}
                >
                  {page.title}
                </ListItem>
              ))}
            </List>
          </Stack>
          <Switch
            inputProps={{
              "aria-label": "Helooooooooooooooooooooooooooooooooo",
            }}
            defaultChecked
            color="default"
            onChange={()=>dispatch(setDarkMode())}
            sx={{ ml: "auto" }}
          />
          <Button component={NavLink} to={"/basket"}>
            <Badge hidden  badgeContent={countItems} color="secondary">
              <ShoppingBag  color="disabled"></ShoppingBag>
            </Badge>
          </Button>
        </Toolbar>
      </Container>
    </AppBarYB>
    </HideOnScroll>
  );
}
export default Header;
