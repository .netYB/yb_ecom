import { ThemeProvider } from "@emotion/react";
import { CssBaseline, Container } from "@mui/material";
import { useEffect, useState } from "react";
import { Outlet } from "react-router-dom";
import { ToastContainer } from "react-toastify";
import Header from "./Header/Header";
import "react-toastify/dist/ReactToastify.css";
import agent from "../api/agent";
import { getCookie } from "../Utils/Utils";
import Loading from "./Loading";
import { useTypeDispatch, useTypeSelector } from "../context/StoreReduxConfig";
import { setBasket } from "../context/Slices/basketSlice";
import themea from "../Utils/themea";
import themeb from "../Utils/themeb";
import UseEventListener from "../hooks/UseEventListener";
import { setDefaultTheme } from "../context/Slices/darkModeSlice";
import FooterYB from "./Footer/FooterYB";


function App() {
  const dispatch = useTypeDispatch();
  const { darkMode } = useTypeSelector((state) => state.darkMode);
  const [loading, setLoading] = useState(true);

  useEffect(() => {
    const buyerId = getCookie("buyerId");
    if (buyerId) {
      agent.Basket.get()
        .then((basket) => dispatch(setBasket(basket)))
        .catch((error) => console.log(error))
        .finally(() => setLoading(false));
    } else setLoading(false);
    dispatch(setDefaultTheme());
  }, [dispatch]);

  // Changing theme with IOS/Windows theme
  UseEventListener(
    "change",
    () => dispatch(setDefaultTheme()),
    window.matchMedia("(prefers-color-scheme: dark)")
  );
  if (loading) return <Loading message="Initialising app..." />;
  return (
    <ThemeProvider theme={darkMode ? themeb : themea}>
      <ToastContainer position="bottom-right" hideProgressBar theme="colored" />
      <CssBaseline />
      <Header />
      <Container sx={{ marginTop: "80px" }}>
        <Outlet />
      </Container>
      <FooterYB/>
    </ThemeProvider>
  );
}

export default App;
