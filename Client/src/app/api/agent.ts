import axios, { AxiosError, AxiosResponse } from "axios";
import { toast } from "react-toastify";
import { PaginatedResponse } from "../models/pagination";
import { router } from "../router/Routes";


axios.defaults.baseURL = 'http://localhost:5203/api/'
axios.defaults.withCredentials = true;

const responseBody = (response: AxiosResponse) => response.data;
const sleep = () => new Promise(resolve => setTimeout(resolve, 2500))

axios.interceptors.response.use(async response => {
    await sleep();
    // Adding custom Headers to response
    const pagination = response.headers['pagination']
    if (pagination) {
        // on format data from [] of items to items [] + metaData
        response.data = new PaginatedResponse(response.data, JSON.parse(pagination))
        return response
    }
    // now response return two item { list of items and a metaData}
    return response
}, (error: AxiosError) => {
    const { data, status } = error.response! as AxiosResponse;
    switch (status) {
        case 400:
            if (data.errors) {
                const ErrorsValidation: string[] = [];
                for (const key in data.errors) {
                    if (data.errors[key]) {
                        ErrorsValidation.push(data.errors[key])
                    }
                }
                throw ErrorsValidation.flat();
            }
            toast.error(data.status + ":" + data.title)
            break;
        case 401:
            toast.error(data.status + ":" + data.title)
            break;
        case 404:
            toast.error(data.status + ":" + data.title)
            break;
        case 500:
            router.navigate('/404', { state: data })
            break;
        default:
            break;
    };
    return Promise.reject(error.response?.data)
}   // Adding return pour ne pas crash
)

const requests = {
    // UrlSearchParams pour passer les params 
    get: (url: string, params?: URLSearchParams) => axios.get(url, { params }).then(responseBody),
    post: (url: string, body: {}) => axios.post(url).then(responseBody),
    put: (url: string, body: {}) => axios.put(url).then(responseBody),
    delete: (url: string) => axios.delete(url).then(responseBody),
}

const Catalog = {
    // On passe parmas de filter (more details dans les slices)
    list: (params: URLSearchParams) => requests.get("products", params),
    details: (id: number) => requests.get(`products/${id}`),
    fetchFilters: () => requests.get("products/filters"),
}

const Basket = {
    get: () => requests.get("basket"),
    // {} is for body
    addItem: (productId: number, quantity = 1) => requests.post(`basket?productId=${productId}&quantity=${quantity}`, {}),
    removeItem: (productId: number, quantity = 1) => requests.delete(`basket?productId=${productId}&quantity=${quantity}`)
}

const TestErrors = {
    get400Error: () => requests.get("buggy/bad-request"),
    get404Error: () => requests.get("buggy/not-found"),
    get401Error: () => requests.get("buggy/unauthorise"),
    get500Error: () => requests.get("buggy/server-error"),
    getValidationError: () => requests.get("buggy/validation-error"),
}

const agent = {
    Catalog,
    Basket,
    TestErrors
}

export default agent;