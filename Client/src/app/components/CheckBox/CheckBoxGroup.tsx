import { Checkbox, FormControlLabel, FormGroup, Skeleton } from "@mui/material";
import { useState } from "react";

interface Props {
  items: string[];
  checked?: string[];
  filtersLoaded: boolean;
  onChange: (item: string[]) => void;
}

export const CheckBoxGroup = ({
  items,
  checked,
  filtersLoaded,
  onChange,
}: Props) => {
  const [checkedItems, setCheckedItems] = useState(checked || []);

  // Function pour displaye checked and inChecked
  const handleChecked = (value: string) => {
    // recherche l'item 
    const currentIndex = checkedItems.findIndex((item) => item === value);
    let newChecked: string[] = [];
    // -1 si il nya pas 
    // ET on cree un nouveau
    if (currentIndex === -1) newChecked = [...checkedItems, value];
    // Sinon on le supprime
    else newChecked = checkedItems.filter((item) => item !== value);
    // Update list
    setCheckedItems(newChecked);
    // Function de change pour lancer l'api
    onChange(newChecked);
  };
  return (
    <FormGroup>
      {filtersLoaded ? (
        items.map((item) => {
          return (
            <FormControlLabel
              key={item}
              control={
                <Checkbox
                  checked={checkedItems.indexOf(item) != -1}
                  onClick={() => handleChecked(item)}
                />
              }
              label={item}
            />
          );
        })
      ) : (
        <>
          <Skeleton variant="rectangular" sx={{ mb: 2 }} />
          <Skeleton variant="rectangular" sx={{ mb: 2 }} />
          <Skeleton variant="rectangular" sx={{ mb: 2 }} />
          <Skeleton variant="rectangular" sx={{ mb: 2 }} />
          <Skeleton variant="rectangular" sx={{ mb: 2 }} width="60%" />
        </>
      )}
    </FormGroup>
  );
};
