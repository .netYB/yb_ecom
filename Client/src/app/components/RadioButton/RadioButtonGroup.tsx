import {
  FormControl,
  RadioGroup,
  FormControlLabel,
  Radio,
} from "@mui/material";

interface Props {
  options: any[];
  onChange: (event: any) => void;
  selectedValue: string;
}

const RadioButtonGroup = ({ options, selectedValue, onChange }: Props) => {
  return (
    <FormControl>
      <RadioGroup onChange={onChange} value={selectedValue}>
        {options.map((sortOption) => (
          <div key={sortOption.value}>
            <FormControlLabel
              value={sortOption.value}
              control={<Radio />}
              label={sortOption.label}
            />
          </div>
        ))}
      </RadioGroup>
    </FormControl>
  );
};

export default RadioButtonGroup;
