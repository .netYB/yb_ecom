import { Box, Typography, Pagination } from "@mui/material";
import { MetaData } from "../../models/pagination";

interface Props {
  metaData: MetaData;
  onPageChange: (page: number) => void;
}

const PaginationGroup = ({ metaData, onPageChange }: Props) => {
  const { currentPage, totalCount, pageSize, totalPages } = metaData;
  return (
    <Box display={"flex"} alignContent="center" justifyContent="space-between">
      <Typography>
        Displaying{' '}
        {currentPage * pageSize > totalCount
          ? totalCount
          : currentPage * pageSize}{" "}
        of {totalCount} results
      </Typography>
      <Pagination
        sx={{ alignContent: "end" }}
        count={totalPages}
        page={currentPage}
        variant="outlined"
        color="primary"
        onChange={(_,page)=> onPageChange(page)}
      />
    </Box>
  );
};

export default PaginationGroup;
