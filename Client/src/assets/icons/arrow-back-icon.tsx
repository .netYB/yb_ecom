import { SvgIconProps } from "../../app/types/types";


export function ArrowBackIcon(props: SvgIconProps) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="6.811" height="12.121" viewBox="0 0 6.811 12.121" {...props}>
      <path
        id="Path_5146"
        d="M10,5,5,0,0,5"
        transform="translate(0.75 11.061) rotate(-90)"
        fill="none"
        stroke="#232323"
        strokeLinecap="round"
        strokeLinejoin="round"
        strokeMiterlimit="10"
        strokeWidth="1.5"
      ></path>
    </svg>
  )
}
