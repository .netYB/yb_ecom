import { SvgIconProps } from "../../app/types/types";


export function InstagramIcon(props: SvgIconProps) {
  return (
    <svg
      id="Groupe_23"
      xmlns="http://www.w3.org/2000/svg"
      width="21.281"
      height="21.281"
      viewBox="0 0 21.281 21.281"
      {...props}
    >
      <path
        id="Tracé_76"
        d="M14.631,0H6.65A6.65,6.65,0,0,0,0,6.65v7.98a6.65,6.65,0,0,0,6.65,6.65h7.98a6.65,6.65,0,0,0,6.65-6.65V6.65A6.65,6.65,0,0,0,14.631,0Zm4.655,14.631a4.66,4.66,0,0,1-4.655,4.655H6.65A4.66,4.66,0,0,1,2,14.631V6.65A4.66,4.66,0,0,1,6.65,2h7.98A4.66,4.66,0,0,1,19.286,6.65Z"
        fill="#fff"
      ></path>
      <path
        id="Tracé_77"
        d="M9.32,4a5.32,5.32,0,1,0,5.32,5.32A5.32,5.32,0,0,0,9.32,4Zm0,8.645A3.325,3.325,0,1,1,12.645,9.32,3.329,3.329,0,0,1,9.32,12.645Z"
        transform="translate(1.32 1.32)"
        fill="#fff"
      ></path>
      <ellipse
        id="Ellipse_7"
        cx="0.709"
        cy="0.709"
        rx="0.709"
        ry="0.709"
        transform="translate(15.651 4.212)"
        fill="#fff"
      ></ellipse>
    </svg>
  )
}
