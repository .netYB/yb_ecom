import { SvgIconProps } from "../../app/types/types";


export function MenuCloseIcon(props: SvgIconProps) {
  return (
    <svg xmlns="http://www.w3.org/2000/svg" width="24.522" height="19.6" viewBox="0 0 24.522 19.6" {...props}>
      <g id="burger" transform="translate(1.8 1.8)">
        <g id="Groupe_7842" transform="translate(0 -1.564)">
          <path
            id="Tracé_7041"
            d="M14.171,0h.8"
            transform="translate(-4.112 17.564)"
            fill="none"
            stroke="#ad2184"
            strokeLinecap="round"
            strokeWidth="3.6"
          ></path>
          <path
            id="Tracé_7314"
            d="M0,0H8.444"
            transform="translate(6.239 9.564)"
            fill="none"
            stroke="#ad2184"
            strokeLinecap="round"
            strokeWidth="3.6"
          ></path>
          <line
            id="Ligne_1241"
            x2="20.922"
            transform="translate(0 1.564)"
            fill="none"
            stroke="#ad2184"
            strokeLinecap="round"
            strokeWidth="3.6"
          ></line>
        </g>
      </g>
    </svg>
  )
}
