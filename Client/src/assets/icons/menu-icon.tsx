import { SvgIconProps } from "../../app/types/types";


export function MenuIcon(props: SvgIconProps) {
  return (
    <svg width="24" height="20" viewBox="0 0 24 20" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <rect y="0.577148" width="24" height="4" rx="2" fill="#AD2184" />
      <rect x="7" y="7.57715" width="17" height="4" rx="2" fill="#AD2184" />
      <rect x="11" y="15.5771" width="13" height="4" rx="2" fill="#AD2184" />
    </svg>
  )
}
