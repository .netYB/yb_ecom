import { SvgIconProps } from "../../app/types/types";


export function CheckboxIcon(props: SvgIconProps) {
  return (
    <svg width="13" height="9" viewBox="0 0 13 9" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <g clipPath="url(#clip0_4585_3764)">
        <path
          fillRule="evenodd"
          clipRule="evenodd"
          d="M12.143 1.76L4.206 8.424L0 4.218L1.577 2.641L4.206 5.27L10.722 0L12.143 1.76Z"
          fill="white"
        />
      </g>
      <defs>
        <clipPath id="clip0_4585_3764">
          <rect width="12.143" height="8.424" fill="white" />
        </clipPath>
      </defs>
    </svg>
  )
}
