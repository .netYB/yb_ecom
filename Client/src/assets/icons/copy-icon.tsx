import { SvgIconProps } from "../../app/types/types";


export function CopyIcon(props: SvgIconProps) {
  return (
    <svg width="12" height="12" viewBox="0 0 12 12" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M2 1C1.4475 1 1 1.4475 1 2V9H2V2H9V1H2ZM4 3C3.4475 3 3 3.4475 3 4V10C3 10.5525 3.4475 11 4 11H10C10.5525 11 11 10.5525 11 10V4C11 3.4475 10.5525 3 10 3H4ZM4 4H10V10H4V4Z"
        fill="#AD2184"
      />
    </svg>
  )
}
