import { SvgIconProps } from "../../app/types/types";


export function ArrowDownIcon(props: SvgIconProps) {
  return (
    <svg width="10" height="6" viewBox="0 0 10 6" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <path
        d="M8.53306 1.42749L4.97406 4.98649L1.41406 1.42749"
        stroke="white"
        strokeWidth="2"
        strokeMiterlimit="10"
        strokeLinecap="round"
        strokeLinejoin="round"
      />
    </svg>
  )
}
