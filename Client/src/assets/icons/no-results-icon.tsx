import { SvgIconProps } from "../../app/types/types";


export function NotResultsIcon(props: SvgIconProps) {
  return (
    <svg width="1062" height="870" viewBox="0 0 1062 870" fill="none" xmlns="http://www.w3.org/2000/svg" {...props}>
      <g id="No Results">
        <g id="circle">
          <path id="Stroke 1" d="M35.5378 399V408" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <g id="Group 8">
            <path
              id="Stroke 2"
              d="M15.323 403.875L22.6773 411.446"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
            <path
              id="Stroke 4"
              d="M6.72144 424.028H17.1203"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
            <path
              id="Stroke 6"
              d="M14.4792 444.539L21.8335 436.969"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
          </g>
          <path id="Stroke 9" d="M35.5378 447V438" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 10" d="M57 444L51 438" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 11" d="M62.5378 426H53.5378" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path
            id="Stroke 12"
            d="M56.5378 405L50.5378 411"
            stroke="#D2D8DF"
            strokeWidth="2.921"
            strokeLinecap="round"
          />
          <path id="Stroke 13" d="M518.538 6V12" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 15" d="M500.538 24H506.538" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 16" d="M518.538 42V36" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 17" d="M533.538 24H527.538" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 18" d="M170.538 258V267" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 19" d="M143.538 285H152.538" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 20" d="M170.538 309V300" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <path id="Stroke 21" d="M194.538 285H185.538" stroke="#D2D8DF" strokeWidth="2.921" strokeLinecap="round" />
          <g id="Group 42">
            <path
              id="Stroke 22"
              d="M816.299 585.117C823.469 585.117 829.283 590.933 829.283 598.105C829.283 605.278 823.469 611.092 816.299 611.092C809.13 611.092 803.315 605.278 803.315 598.105"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
            <path
              id="Fill 26"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M875.719 467.29C883.429 464.885 895.246 455.457 897.982 445.312C900.462 454.28 910.861 464.885 920.244 465.688C909.682 469.428 899.584 480.537 897.982 489.265C896.892 480.369 882.925 468.587 875.719 467.29Z"
              fill="#EBECEE"
            />
            <path
              id="Fill 28"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M84.3523 581.116C88.2083 579.914 94.1145 575.201 95.4821 570.129C96.7225 574.61 101.921 579.914 106.612 580.317C101.332 582.184 96.2844 587.74 95.4821 592.104C94.9371 587.656 87.9536 581.764 84.3523 581.116Z"
              fill="#EBECEE"
            />
            <path
              id="Fill 30"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M155.91 163.791C155.91 169.355 151.402 173.862 145.842 173.862C140.28 173.862 135.774 169.355 135.774 163.791C135.774 158.23 140.28 153.721 145.842 153.721C151.402 153.721 155.91 158.23 155.91 163.791Z"
              fill="#D1D8DF"
            />
            <path
              id="Fill 32"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M828.195 190.832C828.195 200.128 820.659 207.663 811.368 207.663C802.071 207.663 794.538 200.128 794.538 190.832C794.538 181.536 802.071 174 811.368 174C820.659 174 828.195 181.536 828.195 190.832Z"
              fill="#EBECEE"
            />
            <path
              id="Fill 34"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M1061.28 145.229C1061.28 152.604 1055.3 158.58 1047.93 158.58C1040.56 158.58 1034.58 152.604 1034.58 145.229C1034.58 137.853 1040.56 131.877 1047.93 131.877C1055.3 131.877 1061.28 137.853 1061.28 145.229Z"
              fill="#EBECEE"
            />
            <path
              id="Stroke 36"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M344.068 768.803C344.068 773.452 340.301 777.22 335.653 777.22C331.008 777.22 327.241 773.452 327.241 768.803C327.241 764.154 331.008 760.386 335.653 760.386C340.301 760.386 344.068 764.154 344.068 768.803Z"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
            <path
              id="Stroke 38"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M339.062 99.0948C339.062 104.659 334.554 109.165 328.994 109.165C323.434 109.165 318.927 104.659 318.927 99.0948C318.927 93.5336 323.434 89.0244 328.994 89.0244C334.554 89.0244 339.062 93.5336 339.062 99.0948Z"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
            <path
              id="Stroke 40"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M595.87 862.008C589.437 864.457 582.237 861.224 579.79 854.791C577.342 848.356 580.571 841.152 587.007 838.706C593.438 836.258 600.638 839.488 603.086 845.923C605.536 852.358 602.304 859.56 595.87 862.008Z"
              stroke="#D2D8DF"
              strokeWidth="2.921"
              strokeLinecap="round"
            />
          </g>
        </g>
        <g id="search">
          <g id="documents">
            <path
              id="Fill 1"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M592.035 624H346.021C328.706 624 314.532 609.784 314.532 592.41V241.582C314.532 224.209 328.706 210 346.021 210H592.035C609.358 210 623.532 224.209 623.532 241.582V592.41C623.532 609.784 609.358 624 592.035 624Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 3"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M571.035 639H325.021C307.706 639 293.532 624.784 293.532 607.41V256.582C293.532 239.209 307.706 225 325.021 225H571.035C588.358 225 602.532 239.209 602.532 256.582V607.41C602.532 624.784 588.358 639 571.035 639Z"
              fill="#E8EBF2"
            />
            <path
              id="Fill 5"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M550.609 650.841H304.087C286.735 650.841 272.532 636.685 272.532 619.383V270.007C272.532 252.705 286.735 238.556 304.087 238.556H550.609C567.968 238.556 582.171 252.705 582.171 270.007V619.383C582.171 636.685 567.968 650.841 550.609 650.841Z"
              fill="#D8DBEA"
            />
            <path
              id="Fill 8"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M550.609 650.841H304.087C286.735 650.841 272.532 636.685 272.532 619.383V270.007C272.532 252.705 286.735 238.556 304.087 238.556H550.609C567.968 238.556 582.171 252.705 582.171 270.007V619.383C582.171 636.685 567.968 650.841 550.609 650.841Z"
              fill="#F1F2F7"
            />
            <path
              id="Fill 11"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M515.075 345H332.989C327.789 345 323.532 340.951 323.532 336.003C323.532 331.049 327.789 327 332.989 327H515.075C520.275 327 524.532 331.049 524.532 336.003C524.532 340.951 520.275 345 515.075 345Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 13"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M515.075 399H332.989C327.789 399 323.532 394.951 323.532 390.003C323.532 385.049 327.789 381 332.989 381H515.075C520.275 381 524.532 385.049 524.532 390.003C524.532 394.951 520.275 399 515.075 399Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 15"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M515.075 453H332.989C327.789 453 323.532 448.951 323.532 444.003C323.532 439.056 327.789 435 332.989 435H515.075C520.275 435 524.532 439.056 524.532 444.003C524.532 448.951 520.275 453 515.075 453Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 17"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M515.075 507H332.989C327.789 507 323.532 502.951 323.532 498.003C323.532 493.049 327.789 489 332.989 489H515.075C520.275 489 524.532 493.049 524.532 498.003C524.532 502.951 520.275 507 515.075 507Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 19"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M428.035 561H333.029C327.807 561 323.532 556.951 323.532 552.003C323.532 547.049 327.807 543 333.029 543H428.035C433.257 543 437.532 547.049 437.532 552.003C437.532 556.951 433.257 561 428.035 561Z"
              fill="#E0E2EE"
            />
          </g>
          <g id="search_2">
            <path
              id="Fill 21"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M619.797 491.21C583.387 527.614 527.019 531.721 486.097 503.615C480.867 499.958 475.89 495.851 471.248 491.21C468.858 488.829 466.606 486.371 464.483 483.783C460.238 478.615 456.52 473.104 453.474 467.409C448.107 457.791 444.457 447.585 442.465 437.036C436.036 403.952 445.648 368.342 471.248 342.755C496.908 317.091 532.524 307.542 565.613 313.908C576.157 315.962 586.372 319.61 595.992 324.916C601.695 328.03 607.131 331.748 612.307 335.992C614.888 338.106 617.346 340.366 619.728 342.755C624.378 347.396 628.546 352.366 632.135 357.602C660.253 398.51 656.138 454.875 619.797 491.21Z"
              fill="white"
              fillOpacity="0.1"
            />
            <path
              id="Fill 23"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M609.608 484.041C571.729 521.927 510.342 521.92 472.455 484.041C434.617 446.203 434.617 384.809 472.504 346.929C510.342 309.091 571.729 309.091 609.566 346.929C647.446 384.809 647.446 446.203 609.608 484.041ZM629.049 327.467C580.427 278.844 501.636 278.844 453.014 327.467C404.399 376.082 404.357 454.921 452.972 503.543C497.224 547.781 566.583 551.771 615.367 515.449C620.132 511.896 624.721 507.914 629.098 503.543C633.468 499.166 637.451 494.577 640.996 489.813C677.318 441.021 673.294 371.711 629.049 327.467Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 25"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M820.398 686.825L818.357 688.866C804.852 702.378 782.738 702.378 769.226 688.866L644.532 564.172L695.704 513L820.398 637.694C833.91 651.206 833.91 673.313 820.398 686.825Z"
              fill="url(#paint0_linear_5430_59288)"
            />
            <path
              id="Fill 27"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M643.684 486L680.532 522.848L654.372 549L617.532 512.152C622.394 508.527 627.069 504.463 631.543 500.004C636.003 495.545 640.066 490.862 643.684 486Z"
              fill="#E0E2EE"
            />
            <path
              id="Fill 29"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M818.532 636.161L767.686 687L758.532 677.839L809.371 627L818.532 636.161Z"
              fill="#DE82C3"
            />
            <path
              id="Fill 31"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M698.532 514.76L646.293 567L638.532 559.24L690.771 507L698.532 514.76Z"
              fill="url(#paint1_linear_5430_59288)"
            />
            <path
              id="Fill 33"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M584.532 333.896L456.421 462C451.597 453.353 448.316 444.178 446.532 434.694L557.224 324C566.702 325.853 575.885 329.126 584.532 333.896Z"
              fill="white"
              fillOpacity="0.5"
            />
            <path
              id="Fill 35"
              fillRule="evenodd"
              clipRule="evenodd"
              d="M626.532 364.345L488.031 495C483.071 491.735 478.35 488.06 473.947 483.907C471.681 481.769 469.538 479.576 467.532 477.26L607.72 345C610.175 346.899 612.507 348.921 614.766 351.059C619.168 355.212 623.129 359.659 626.532 364.345Z"
              fill="white"
              fillOpacity="0.5"
            />
          </g>
        </g>
      </g>
      <defs>
        <linearGradient
          id="paint0_linear_5430_59288"
          x1="737.532"
          y1="420"
          x2="551.532"
          y2="606"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#A8498C" />
          <stop offset="1" stopColor="#331C40" />
        </linearGradient>
        <linearGradient
          id="paint1_linear_5430_59288"
          x1="668.532"
          y1="477"
          x2="608.532"
          y2="537"
          gradientUnits="userSpaceOnUse"
        >
          <stop stopColor="#BA2B90" />
          <stop offset="1" stopColor="#FF62A5" />
        </linearGradient>
      </defs>
    </svg>
  )
}
