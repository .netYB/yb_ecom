import React from "react";
import ReactDOM from "react-dom/client";
import { Provider } from "react-redux";
import { RouterProvider } from "react-router-dom";
import { storeRedux } from "./app/context/StoreReduxConfig";
import { router } from "./app/router/Routes";
import './scss/styles.css'

ReactDOM.createRoot(document.getElementById("root") as HTMLElement).render(
  <React.StrictMode>
      <Provider store={storeRedux}>
        <RouterProvider router={router} />
      </Provider>
  </React.StrictMode>
);