const { colors } = require("tailwindcss/colors");
const {  keyframes } = require("tailwindcss/defaultTheme");

/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./src/app/**/**/*.{js,ts,jsx,tsx}",
    "./src/app/components/**/*.{js,ts,jsx,tsx}",
    "./src/app/features/**/*.{js,ts,jsx,tsx}",
    "./src/app/layout/**/*.{js,ts,jsx,tsx}",
    "./src/app/**/*.{js,ts,jsx,tsx}",
    "./src/ui/**/*.{js,ts,jsx,tsx}",
    "./src/assets/icons/*.{js,ts,jsx,tsx}",
  ],
  theme: {
    screens: {
      sm: "640px",
      md: "768px",
      lg: "1024px",
      xl: "1190px",
      "2xl": "1536px",
    },
    extend: {
      colors: {
        magenta: "#AD2184",
        "magenta-secondary": "#64357F",
        "magenta-light": "#D533A6",
        error: "#FF3B71",
        ...colors,
      },
      backgroundImage: {
        footer:
          "radial-gradient(37vmax 15vmax at 2% 0%, #ad2184 0%, #5a0d5a 100%)",
        "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
        "gradient-conic":
          "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
        "menu-header": "linear-gradient(270deg, #a7488b, #321b3f)",
      },
      boxShadow: {
        select:
          "rgba(0, 0, 0, 0.2) 0px 5px 5px -3px, rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px",
        nav: "0px 7px 17px #00000029",
      },
      keyframes: {
        gradient: {
          "0%": { "background-position": "0% 50%" },
          "50%": { "background-position": "100% 50%" },
          "100%": { "background-position": "0% 50%" },
        },
        ping: {
          "75%": { transform: "scale(2)", opacity: 0 },
          "100%": { transform: "scale(2)", opacity: 0 },
          ...keyframes,
        },
      },
      boxShadow: {
        sidebar: "0px 2px 5px rgba(56, 27, 73, 0.14)",
        select:
          "rgba(0, 0, 0, 0.2) 0px 5px 5px -3px, rgba(0, 0, 0, 0.14) 0px 8px 10px 1px, rgba(0, 0, 0, 0.12) 0px 3px 14px 2px",
        nav: "0px 7px 17px #00000029",
      },
    },
  },
  plugins: [],
};
